package com.example.book_store.db.jpa;

import com.example.book_store.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    Book findByAuthorAndTitle(String author, String title);
    Book findById(long id);
    Set<Book> findByAuthor(String author);

}
