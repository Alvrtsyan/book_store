package com.example.book_store.services;

import com.example.book_store.db.jpa.BookRepository;
import com.example.book_store.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class BookService {

    @Autowired
    BookRepository bookRepository;

    @Transactional
    public boolean addBook(String author, String title, int count, int price){
        Book existingBook = bookRepository.findByAuthorAndTitle(author, title);
        if( existingBook != null){
            if(existingBook.getPrice() != price){
                return false;
            }
            existingBook.setCount(existingBook.getCount() + count);
            return true;
        }
        Book book = new Book(author, title, count, price);
        bookRepository.save(book);
        return true;
    }

    @Transactional
    public void updateBook(long id, String newAuthor, String newTitle, int newCount, int newPrice){
        Book book = bookRepository.findById(id);
        book.setAuthor(newAuthor);
        book.setTitle(newTitle);
        book.setCount(newCount);
        book.setPrice(newPrice);
        bookRepository.save(book);
    }

    @Transactional
    public void deleteBook(long id){
        Book book = bookRepository.findById(id);
        bookRepository.delete(book);
    }

    public Set<Book> findByAuthor(String author){
        return bookRepository.findByAuthor(author);
    }
}
