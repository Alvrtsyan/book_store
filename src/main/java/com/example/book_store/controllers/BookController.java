package com.example.book_store.controllers;

import com.example.book_store.entities.Book;
import com.example.book_store.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/v1/bookStore")
public class BookController {

    @Autowired
    private BookService bookService;


    @PostMapping("/addBook")
    public String addBook(@RequestParam String author, @RequestParam String title, @RequestParam int count, @RequestParam int price){
        if(!bookService.addBook(author, title, count, price)){
            return "This book already exists with another price, please update the old one, or change the price";
        }
        return "Book has been added";
    }

    @PostMapping("/updateBook")
    public String updateBook(@RequestParam long id, @RequestParam String author, @RequestParam String title,
                             @RequestParam int count, @RequestParam int price){
        bookService.updateBook(id, author, title, count, price);
        return "Book has been updated";
    }

    @GetMapping("/deleteBook")
    public String deleteBook(@RequestParam long id){
        bookService.deleteBook(id);
        return "Book has been deleted";
    }

    @GetMapping("/findByAuthor")
    public Set<Book> findByAuthor(@RequestParam String author){
        return bookService.findByAuthor(author);
    }
}
